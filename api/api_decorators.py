# -*- coding:utf-8 -*-

from __future__ import unicode_literals
from api.api_tools import bad_response


# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# CHECK IF REQUEST METHOD IS POST => S T A R T
def api_require_post(view_func):
	def check_api_require_post(request):
		if request.method == "POST":
			return view_func(request)
		else:
			return bad_response("Wrong request method")
	return check_api_require_post
# CHECK IF REQUEST METHOD IS POST => E N D
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# CHECK IF PHONE IS NOT NONE IN REQUEST BODY AND IS VALID => S T A R T
def api_require_valid_phone(view_func):
	def check_api_require_fb_token(request):
		if request.POST.get('phone', None) is not None:
			try:
				int(request.POST.get('phone'))
			except:
				return bad_response("Phone number should contain only integers")

			return view_func(request)
		else:
			return bad_response("phone can not be blank")
	return check_api_require_fb_token
# CHECK IF PHONE IS NOT NONE IN REQUEST BODY AND IS VALID => E N D
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-




