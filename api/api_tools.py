# -*- coding:utf-8 -*-

from __future__ import unicode_literals
from django.http import HttpResponse
from django.conf import settings as app_settings
import json
import warnings

warnings.filterwarnings('ignore', message='Unverified HTTPS request')

# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# GENERAL RESPONSE WITH SUCCESS FALSE AND MESSAGE => S T A R T
def bad_response(msg_txt=None, as_response=False):
	response = {
		"success": False
	}
	if msg_txt is not None:
		print(msg_txt)
		response['msg_txt'] = msg_txt
	return api_return_json_dumps(response)
# GENERAL RESPONSE WITH SUCCESS FALSE AND MESSAGE => S T A R T
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# CONVERT JSON DICT TO DUMPS OR THROUGH AN ERROR => S T A R T
def api_return_json_dumps(json_dict):
	try:
		return HttpResponse(json.dumps(json_dict, indent=4), content_type="application/json")
	except Exception as err:
		print(err)
		return bad_response("Unable to return json dumps")
# CONVERT JSON DICT TO DUMPS OR THROUGH AN ERROR => E N D
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
