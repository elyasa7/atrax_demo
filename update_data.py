import os
import time
import requests
import warnings
from csv import reader
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "atrax_demo.settings")
application = get_wsgi_application()

from phone_info.models import PhoneOperator, PhoneInfo, PhoneCity
from django.db import transaction

warnings.filterwarnings('ignore', message='Unverified HTTPS request')

TEMP_DIR = "tmp/"

DATA_LINKS = [
	"https://rossvyaz.gov.ru/data/ABC-3xx.csv",
	"https://rossvyaz.gov.ru/data/ABC-4xx.csv",
	"https://rossvyaz.gov.ru/data/ABC-8xx.csv",
	"https://rossvyaz.gov.ru/data/DEF-9xx.csv"
]


def proceed_phone(data):
	try:
		splitted = data.split(";")
		phone_code = splitted[0]
		phone_start = splitted[1]
		phone_end = splitted[2]
		capacity =  splitted[3]
		operator = splitted[4]
		region = splitted[5]

		# - - - - - - - - - - - - - - - - - -
		# check if operator exists => S T A R T
		try:
			phone_operator = PhoneOperator.objects.get(
				phone_operator_title=operator
			)
		except:
			phone_operator = PhoneOperator.objects.create(
				phone_operator_title=operator
			)
		# check if operator exists => E N D
		# - - - - - - - - - - - - - - - - - -

		# - - - - - - - - - - - - - - - - - -
		# check if city exists => S T A R T
		try:
			region_city_id = PhoneCity.objects.get(
				phone_city_code=phone_code,
				phone_city_title=region
			).id
		except:
			region_city_id = PhoneCity.objects.create(
				phone_city_code=phone_code,
				phone_city_title=region
			).id
		# check if city exists => E N D
		# - - - - - - - - - - - - - - - - - -

		# - - - - - - - - - - - - - - - - - -
		# check if phone exists => S T A R T
		try:
			phone_info = PhoneInfo.objects.get(
				phone_info_code=phone_code,
				phone_info_start=phone_start,
				phone_info_end=phone_end,
				phone_info_city_id=region_city_id
			)
			if phone_info.phone_info_operator.id != phone_operator.id:
				with transaction.atomic():
					phone_info = PhoneInfo.objects.select_for_update().get(
						id=phone_info.id
					)
					phone_info.phone_info_log.append({
						"operator": phone_info.phone_info_operator.phone_operator_title,
						"updated": int(time.time())
					})
					phone_info.phone_info_operator_id = phone_operator.id
					phone_info.save(update_fields=['phone_info_log', 'phone_info_operator'])

		except:
			phone_info = PhoneInfo.objects.create(
				phone_info_code=phone_code,
				phone_info_start=phone_start,
				phone_info_end=phone_end,
				phone_info_capacity=capacity,
				phone_info_operator_id=phone_operator.id,
				phone_info_city_id=region_city_id,
				phone_info_last_update=int(time.time()),
				phone_info_log=[{
					"operator": phone_operator.phone_operator_title,
					"updated": int(time.time())
				}]
			)
		# check if region exists => E N D
		# - - - - - - - - - - - - - - - - - -
	except Exception as err:
		print(err)
		print(data)

def proceed_files():
	for data in DATA_LINKS:
		try:
			file_name = data.split('/')[-1]

			temp_file = open(TEMP_DIR + file_name, "wb")
			temp_file.write(requests.get(data, verify=False).text.encode("UTF-8"))
			temp_file.close()

			total_numbers = 0
			with open(TEMP_DIR + file_name, "r") as info_file:
				content = reader(info_file)
				content_header = next(content)

				if content_header is not None:
					for line in content:
						total_numbers += 1
						try:
							proceed_phone(line[0])
						except Exception as err:
							print(err)
							print(line)

		except Exception as err:
			print(err)

if __name__ == "__main__":
	proceed_files()