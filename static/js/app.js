$(document).foundation();


$(document).on('click', '.search-button', function () {
	var phone = $('.search-input').val();
	var result_wrapper = $('.search-result');
	result_wrapper.html("");
	if (phone !== "") {
		$.ajax({
			url: '/api/',
			type: 'POST',
			data: {
				"phone": 7 + phone
			},
			success: function (_data) {
				if (_data.success) {
					var content = "<h3>Результат поиска</h3>";
					content += "<p><b>Номер телефона: </b>"  + _data.data.number + "</p>";
					content += "<p><b>Оператор: </b>"  + _data.data.operator + "</p>";
					content += "<p><b>Город: </b>"  + _data.data.location + "</p>";

					result_wrapper.html(content);
				} else {
					result_wrapper.html("<h3>Ошибка</h3><p>"  + _data.msg_txt + "</p>");
				}
			},
			error: function () {
				result_wrapper.html("<h1>Что-то пошло не так</h1>");
			}
		})
	}
});