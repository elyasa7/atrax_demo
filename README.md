
crontab to be applied:
0 0 * * *  source ~/atrax/venv/bin/activate && cd ~/atrax/atrax_demo/ && python update_data.py

----------------------

Использование через API

Вы можете использовать данный серви с помощью открытого API

В примере ниже вы найдете пример запроса

URL: "http://thishost.com/api/"
METHOD: POST
phone: "73457652332"

Номер телефона не должен содержать пробелов или каких-либо других символов кроме цифр
