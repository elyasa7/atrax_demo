from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from api.api_tools import bad_response, api_return_json_dumps
from api.api_decorators import api_require_post, api_require_valid_phone
from phone_info.models import PhoneOperator, PhoneInfo, PhoneCity

# Create your views here.


def homepage(request):
	args = {}
	return render(request, "main.html", args)

@csrf_exempt
@api_require_post
@api_require_valid_phone
def api_check_phone(request):
	response = {
		"success": False,
		"msg_txt": "Invalid phone format"
	}
	try:
		phone_request = str(request.POST.get("phone"))

		phone_request = phone_request[1:]

		code = phone_request[0:3]
		number = phone_request[3:]
		phone = PhoneInfo.objects.get(
			phone_info_code=code,
			phone_info_start__lte=number,
			phone_info_end__gte=number,
		)

		response = {
			"success": True,
			"data": {
				"number": phone_request,
				"code": code,
				"operator": phone.phone_info_operator.phone_operator_title,
				"location": phone.phone_info_city.phone_city_title,
				"last_update": phone.phone_info_last_update
			}
		}

	except Exception as err:
		print(err)
		response['msg_txt'] = "Phone not found"

	return api_return_json_dumps(response)