from django.apps import AppConfig


class PhoneInfoConfig(AppConfig):
    name = 'phone_info'
