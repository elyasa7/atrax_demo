from django.db import models
from django.contrib.postgres.fields import JSONField
# Create your models here.


class PhoneInfo(models.Model):
	class Meta:
		db_table = "phone_info"

	phone_info_code = models.IntegerField(
		blank=False,
		null=False,
		db_index=True
	)

	phone_info_start = models.IntegerField(
		blank=False,
		null=False,
		db_index=True
	)

	phone_info_end = models.IntegerField(
		blank=False,
		null=False,
		db_index=True
	)

	phone_info_capacity = models.IntegerField(
		blank=False,
		null=False,
	)

	phone_info_operator = models.ForeignKey(
		'PhoneOperator',
		related_name="phone_info_operator_key",
		blank=False,
		null=False,
		on_delete=models.CASCADE
	)

	phone_info_city = models.ForeignKey(
		'PhoneCity',
		related_name="phone_info_city_key",
		blank=False,
		null=False,
		on_delete=models.CASCADE
	)

	phone_info_last_update = models.IntegerField(
		blank=False,
		null=False
	)

	phone_info_log = JSONField()


class PhoneCity(models.Model):
	class Meta:
		db_table = "phone_city"

	phone_city_code = models.IntegerField(
		blank=False,
		null=False,
		db_index=True
	)

	phone_city_title = models.CharField(
		max_length=150,
		blank=False,
		null=False,
		db_index=True
	)


class PhoneOperator(models.Model):
	class Meta:
		db_table = "phone_operator"

	phone_operator_title = models.CharField(
		max_length=300,
		blank=False,
		null=False,
		unique=True,
		db_index=True
	)