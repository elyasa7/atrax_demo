# Generated by Django 2.2 on 2020-11-07 04:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phone_info', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='phoneinfo',
            name='phone_info_last_update',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
