# Generated by Django 2.2 on 2020-11-07 03:14

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PhoneOperator',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone_operator_title', models.CharField(db_index=True, max_length=300, unique=True)),
            ],
            options={
                'db_table': 'phone_operator',
            },
        ),
        migrations.CreateModel(
            name='PhoneRegion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone_region_code', models.IntegerField(db_index=True, max_length=4)),
                ('phone_region_city', models.CharField(db_index=True, max_length=150)),
                ('phone_region_state', models.CharField(blank=True, db_index=True, max_length=300, null=True)),
            ],
            options={
                'db_table': 'phone_region',
            },
        ),
        migrations.CreateModel(
            name='PhoneInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone_info_code', models.IntegerField(db_index=True)),
                ('phone_info_start', models.IntegerField(db_index=True)),
                ('phone_info_end', models.IntegerField(db_index=True)),
                ('phone_info_capacity', models.IntegerField()),
                ('phone_info_log', django.contrib.postgres.fields.jsonb.JSONField()),
                ('phone_info_operator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='phone_info_operator_key', to='phone_info.PhoneOperator')),
                ('phone_info_region', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='phone_info_region_key', to='phone_info.PhoneRegion')),
            ],
            options={
                'db_table': 'phone_info',
            },
        ),
    ]
