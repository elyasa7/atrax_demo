# Generated by Django 2.2 on 2020-11-07 05:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('phone_info', '0005_auto_20201107_0521'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='phonecity',
            name='phone_city_region',
        ),
        migrations.DeleteModel(
            name='PhoneRegion',
        ),
    ]
