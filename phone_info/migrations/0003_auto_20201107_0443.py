# Generated by Django 2.2 on 2020-11-07 04:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phone_info', '0002_phoneinfo_phone_info_last_update'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phoneregion',
            name='phone_region_code',
            field=models.IntegerField(db_index=True),
        ),
    ]
